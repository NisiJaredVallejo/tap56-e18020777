
package Practicas.practica1;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import Practicas.Listener3;

// @author NISI JARED

public class Practica1 extends JFrame{
    JLabel lbl;
    JTextField tf;
    JButton boton1; 
    
    public Practica1(){
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);        
        this.setLayout(new BorderLayout());
                        
        lbl = new JLabel("Escriba un nombre para saludar");
        tf = new JTextField();
        boton1 = new JButton("Saludar");
        
        // Aqui le pegamos el controlador al boton
        boton1.addActionListener((ActionEvent arg0) -> {
        JOptionPane.showMessageDialog(null, "¡¡Hola!! "+tf.getText());
        });
        
        this.add(lbl, BorderLayout.PAGE_START);
        this.add(tf,  BorderLayout.CENTER);
        this.add(boton1, BorderLayout.PAGE_END); 
    
}
public static void main(String args[]){
        Practica1 frm = new Practica1();
        frm.setSize(300,200);
        frm.setVisible(true);        
    }
}