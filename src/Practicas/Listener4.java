
package Practicas;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JFrame;

// @author NISI JARED

public class Listener4 extends JFrame{
    
    JButton boton1;
    public Listener4(){
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        this.setLayout(new BorderLayout());
        boton1 = new JButton("Cerrar");
        
        // Aqui le pegamos el controlador al boton
        boton1.addMouseListener( new MouseAdapter(){                                    
        public void mouseClicked(MouseEvent arg0) {
        System.exit(0);  }
        });
        
        this.add(boton1, BorderLayout.CENTER);
    }
    
    public static void main(String args[]){
        Listener4 frm = new Listener4();
        frm.setSize(300,200);
        frm.setVisible(true);        
    }
}
