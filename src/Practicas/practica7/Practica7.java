
package Practicas.practica7;


import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

 //@author NISI JARED

public class Practica7 extends javax.swing.JFrame {

    Connection conn = null;
    
    public Practica7() {
        initComponents();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        rbtWindows = new javax.swing.JRadioButton();
        rbtLinux = new javax.swing.JRadioButton();
        rbtMac = new javax.swing.JRadioButton();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        chkProgramacion = new javax.swing.JCheckBox();
        chkDiseño = new javax.swing.JCheckBox();
        chkAdministracion = new javax.swing.JCheckBox();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        sldHoras = new javax.swing.JSlider();
        lblHoras = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jLabel1.setText("Elija un sistema oprativo");

        rbtWindows.setSelected(true);
        rbtWindows.setText("Windows");

        rbtLinux.setText("Linux");

        rbtMac.setText("Mac");

        jLabel2.setText("Elija su especialidad");

        chkProgramacion.setText("Programacion");

        chkDiseño.setText("Diseño grafico");

        chkAdministracion.setText("Administracion");

        jLabel3.setText("Horas que dedicas en el ordenador");

        sldHoras.setMaximum(24);
        sldHoras.setValue(0);
        sldHoras.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                sldHorasStateChanged(evt);
            }
        });

        lblHoras.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblHoras.setText("0");

        jButton1.setText("Guardar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(sldHoras, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(24, 24, 24)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(rbtMac)
                                    .addComponent(rbtLinux)
                                    .addComponent(rbtWindows)
                                    .addComponent(jLabel1)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(31, 31, 31)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(chkProgramacion)
                                    .addComponent(jLabel2)
                                    .addComponent(chkDiseño)
                                    .addComponent(chkAdministracion)))
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel3))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(84, 84, 84)
                        .addComponent(lblHoras, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(70, 70, 70)
                        .addComponent(jButton1)))
                .addContainerGap(44, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(rbtWindows)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(rbtLinux)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(rbtMac)
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(chkProgramacion)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(chkDiseño)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(chkAdministracion)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblHoras)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sldHoras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButton1)
                .addContainerGap(19, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        String sWin = "Windows";
        String sProg ="N";
        String sGraf = "N";
        String sAdmon ="N";
        String sHoras="0";
        String sResultado="";
                
        if (this.rbtLinux.isSelected()){
        sWin="Linux";
        }            
        
        sWin=this.rbtMac.isSelected()?"Mac":sWin;
        sProg=this.chkProgramacion.isSelected()?"S":"N";
        sGraf=this.chkDiseño.isSelected()?"S":"N";
        sAdmon=this.chkAdministracion.isSelected()?"S":"N";
        
        sHoras=Integer.toString(this.sldHoras.getValue());
        
        sResultado=sWin+","+sProg+","+sGraf+","+sAdmon+","+sHoras;
        
        this.guardarResultado(sResultado);
        this.guardarResultadoBD(sWin,sProg,sGraf,sAdmon,this.sldHoras.getValue());
        
        JOptionPane.showMessageDialog(this, sResultado);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void sldHorasStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_sldHorasStateChanged
        // TODO add your handling code here:
        this.lblHoras.setText(Integer.toString(this.sldHoras.getValue()));
    }//GEN-LAST:event_sldHorasStateChanged

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:
        if(conn!=null){
        try{
        conn.close();
        }catch (SQLException ex){
        Logger.getLogger(Practica7.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
    }//GEN-LAST:event_formWindowClosing

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Practica7.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Practica7.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Practica7.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Practica7.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        try {
            // The newInstance() call is a work around for some
            // broken Java implementations

            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }   
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Practica7().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox chkAdministracion;
    private javax.swing.JCheckBox chkDiseño;
    private javax.swing.JCheckBox chkProgramacion;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel lblHoras;
    private javax.swing.JRadioButton rbtLinux;
    private javax.swing.JRadioButton rbtMac;
    private javax.swing.JRadioButton rbtWindows;
    private javax.swing.JSlider sldHoras;
    // End of variables declaration//GEN-END:variables

    private void guardarResultado(String sResultado) {
        
        FileWriter fw;
        
        try {
            fw = new FileWriter("encuesta.csv", true);
            fw.write(sResultado+"\n");
            fw.close();
            
        } catch (IOException ex) {
            Logger.getLogger(Practica7.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void guardarResultadoBD(String sWin, String sProg, String sGraf, String sAdmon, int iHoras) {
       
        Statement stnt;
        String insertStatement="";
        
        try {
           if(conn==null){
                conn =
                   DriverManager.getConnection("jdbc:mysql://localhost/encuesta?" +
                                               "useUnicode=true&useJDBCCompliantTimezoneShift=true"+
                                               "&useLegacyDatetimeCode=false&serverTimezone=UTC"+
                                               "&user=encuesta&password=encuesta");            
       } 
           stnt=conn.createStatement();
           
           insertStatement=String.format("INSERT INTO respuestas (sisoper,progra,diseno,admon,horas) values('%s','%s','%s','%s',%d)",sWin,sProg,sGraf,sAdmon,iHoras);
           
           stnt.execute(insertStatement);
           
            } catch (SQLException ex) {
                // handle any errors
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            }     
    }
}
