
package Practicas;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

 // @author NISI JARED

public class Listener3 extends JFrame{
    
    JButton boton1;
    public Listener3(){
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        this.setLayout(new BorderLayout());
        boton1 = new JButton("Cerrar");
        
        // Aqui le pegamos el controlador al boton
        boton1.addActionListener((ActionEvent arg0) -> {
        System.exit(0);
        });
        
        this.add(boton1, BorderLayout.CENTER);
    }
    
    public static void main(String args[]){
        Listener3 frm = new Listener3();
        frm.setSize(300,200);
        frm.setVisible(true);        
    }
}
