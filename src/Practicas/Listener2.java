
package Practicas;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
 
//@author NISI JARED
 
public class Listener2 extends JFrame implements ActionListener {
    
    JButton boton1;
    
    public Listener2(){
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        this.setLayout(new BorderLayout());
        boton1 = new JButton("Cerrar");
        
        // Aqui le pegamos el controlador al boton
        boton1.addActionListener(this);
        
        this.add(boton1, BorderLayout.CENTER);
    }
    public static void main(String args[]){
        Listener2 frm = new Listener2();
        frm.setSize(300,200);
        frm.setVisible(true);        
    }
    @Override
    public void actionPerformed(ActionEvent arg0) {
        System.exit(0);
    }
}
