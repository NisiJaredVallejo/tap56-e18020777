
package Practicas.practica5;

// @author NISI JARED

public class Practica5 extends javax.swing.JFrame {

    
    public Practica5() {
        initComponents();
        this.jComboBox1Espejo.setModel(this.jComboBox1Original.getModel());
        this.jSpinner1Espejo.setModel(this.jSpinner1Original.getModel());
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jCheckBox3Original = new javax.swing.JCheckBox();
        jCheckBox3Espejo = new javax.swing.JCheckBox();
        jTextField1Original = new javax.swing.JTextField();
        jTextField1Espejo = new javax.swing.JTextField();
        jComboBox1Original = new javax.swing.JComboBox<>();
        jComboBox1Espejo = new javax.swing.JComboBox<>();
        jSpinner1Original = new javax.swing.JSpinner();
        jSeparator1 = new javax.swing.JSeparator();
        jSpinner1Espejo = new javax.swing.JSpinner();
        jRadioButton1Original = new javax.swing.JRadioButton();
        jRadioButton1Espejo = new javax.swing.JRadioButton();
        jRadioButton2Original = new javax.swing.JRadioButton();
        jRadioButton2Espejo = new javax.swing.JRadioButton();
        jRadioButton3Original = new javax.swing.JRadioButton();
        jRadioButton3Espejo = new javax.swing.JRadioButton();
        jCheckBox1Original = new javax.swing.JCheckBox();
        jCheckBox1Espejo = new javax.swing.JCheckBox();
        jCheckBox2Original = new javax.swing.JCheckBox();
        jCheckBox2Espejo = new javax.swing.JCheckBox();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jCheckBox3Original.setText("Opcion 6");
        jCheckBox3Original.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox3OriginalActionPerformed(evt);
            }
        });

        jCheckBox3Espejo.setText("Opcion 6");
        jCheckBox3Espejo.setEnabled(false);

        jTextField1Original.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextField1OriginalKeyReleased(evt);
            }
        });

        jTextField1Espejo.setEnabled(false);

        jComboBox1Espejo.setEnabled(false);

        jSpinner1Espejo.setEnabled(false);

        buttonGroup1.add(jRadioButton1Original);
        jRadioButton1Original.setSelected(true);
        jRadioButton1Original.setText("Opcion 1");
        jRadioButton1Original.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton1OriginalActionPerformed(evt);
            }
        });

        buttonGroup2.add(jRadioButton1Espejo);
        jRadioButton1Espejo.setSelected(true);
        jRadioButton1Espejo.setText("Opcion 1");
        jRadioButton1Espejo.setEnabled(false);

        buttonGroup1.add(jRadioButton2Original);
        jRadioButton2Original.setText("Opcion 2");
        jRadioButton2Original.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton2OriginalActionPerformed(evt);
            }
        });

        buttonGroup2.add(jRadioButton2Espejo);
        jRadioButton2Espejo.setText("Opcion 2");
        jRadioButton2Espejo.setEnabled(false);

        buttonGroup1.add(jRadioButton3Original);
        jRadioButton3Original.setText("Opcion 3");
        jRadioButton3Original.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton3OriginalActionPerformed(evt);
            }
        });

        buttonGroup2.add(jRadioButton3Espejo);
        jRadioButton3Espejo.setText("Opcion 3");
        jRadioButton3Espejo.setEnabled(false);

        jCheckBox1Original.setText("Opcion 4");
        jCheckBox1Original.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox1OriginalActionPerformed(evt);
            }
        });

        jCheckBox1Espejo.setText("Opcion 4");
        jCheckBox1Espejo.setEnabled(false);

        jCheckBox2Original.setText("Opcion 5");
        jCheckBox2Original.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox2OriginalActionPerformed(evt);
            }
        });

        jCheckBox2Espejo.setText("Opcion 5");
        jCheckBox2Espejo.setEnabled(false);

        jLabel1.setText("ORIGINAL");

        jLabel2.setText("ESPEJO");

        jLabel3.setText("IMITADOR");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jRadioButton3Original)
                                    .addComponent(jRadioButton2Original)
                                    .addComponent(jRadioButton1Original))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jCheckBox1Original)
                                        .addGap(18, 18, 18)
                                        .addComponent(jTextField1Original, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jCheckBox2Original)
                                        .addGap(18, 18, 18)
                                        .addComponent(jComboBox1Original, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jCheckBox3Original)
                                        .addGap(18, 18, 18)
                                        .addComponent(jSpinner1Original, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jRadioButton3Espejo)
                                    .addComponent(jRadioButton2Espejo)
                                    .addComponent(jRadioButton1Espejo))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jCheckBox1Espejo)
                                        .addGap(18, 18, 18)
                                        .addComponent(jTextField1Espejo, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jCheckBox2Espejo)
                                        .addGap(18, 18, 18)
                                        .addComponent(jComboBox1Espejo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jCheckBox3Espejo)
                                        .addGap(18, 18, 18)
                                        .addComponent(jSpinner1Espejo, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 49, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(100, 100, 100)
                                .addComponent(jLabel3)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButton1Original)
                    .addComponent(jCheckBox1Original)
                    .addComponent(jTextField1Original, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButton2Original)
                    .addComponent(jCheckBox2Original)
                    .addComponent(jComboBox1Original, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButton3Original)
                    .addComponent(jCheckBox3Original)
                    .addComponent(jSpinner1Original, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButton1Espejo)
                    .addComponent(jCheckBox1Espejo)
                    .addComponent(jTextField1Espejo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButton2Espejo)
                    .addComponent(jCheckBox2Espejo)
                    .addComponent(jComboBox1Espejo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButton3Espejo)
                    .addComponent(jCheckBox3Espejo)
                    .addComponent(jSpinner1Espejo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(40, 40, 40))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jCheckBox3OriginalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox3OriginalActionPerformed
        // TODO add your handling code here:
        this.jCheckBox3Espejo.setSelected(this.jCheckBox3Original.isSelected());
    }//GEN-LAST:event_jCheckBox3OriginalActionPerformed

    private void jTextField1OriginalKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1OriginalKeyReleased
        // TODO add your handling code here:
        this.jTextField1Espejo.setText(this.jTextField1Original.getText());

        if (evt.getKeyCode()==10 || evt.getKeyCode()==13 ){
            this.jComboBox1Original.addItem(this.jTextField1Original.getText());
        }
    }//GEN-LAST:event_jTextField1OriginalKeyReleased

    private void jRadioButton1OriginalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton1OriginalActionPerformed
        // TODO add your handling code here:
        this.jRadioButton1Espejo.setSelected(true);
    }//GEN-LAST:event_jRadioButton1OriginalActionPerformed

    private void jRadioButton2OriginalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton2OriginalActionPerformed
        // TODO add your handling code here:
        this.jRadioButton2Espejo.setSelected(true);
    }//GEN-LAST:event_jRadioButton2OriginalActionPerformed

    private void jRadioButton3OriginalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton3OriginalActionPerformed
        // TODO add your handling code here:
        this.jRadioButton3Espejo.setSelected(true);
    }//GEN-LAST:event_jRadioButton3OriginalActionPerformed

    private void jCheckBox1OriginalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox1OriginalActionPerformed
        // TODO add your handling code here:
        this.jCheckBox1Espejo.setSelected(this.jCheckBox1Original.isSelected());
    }//GEN-LAST:event_jCheckBox1OriginalActionPerformed

    private void jCheckBox2OriginalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox2OriginalActionPerformed
        // TODO add your handling code here:
        this.jCheckBox2Espejo.setSelected(this.jCheckBox2Original.isSelected());
    }//GEN-LAST:event_jCheckBox2OriginalActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Practica5.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Practica5.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Practica5.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Practica5.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Practica5().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JCheckBox jCheckBox1Espejo;
    private javax.swing.JCheckBox jCheckBox1Original;
    private javax.swing.JCheckBox jCheckBox2Espejo;
    private javax.swing.JCheckBox jCheckBox2Original;
    private javax.swing.JCheckBox jCheckBox3Espejo;
    private javax.swing.JCheckBox jCheckBox3Original;
    private javax.swing.JComboBox<String> jComboBox1Espejo;
    private javax.swing.JComboBox<String> jComboBox1Original;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JRadioButton jRadioButton1Espejo;
    private javax.swing.JRadioButton jRadioButton1Original;
    private javax.swing.JRadioButton jRadioButton2Espejo;
    private javax.swing.JRadioButton jRadioButton2Original;
    private javax.swing.JRadioButton jRadioButton3Espejo;
    private javax.swing.JRadioButton jRadioButton3Original;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSpinner jSpinner1Espejo;
    private javax.swing.JSpinner jSpinner1Original;
    private javax.swing.JTextField jTextField1Espejo;
    private javax.swing.JTextField jTextField1Original;
    // End of variables declaration//GEN-END:variables
}
