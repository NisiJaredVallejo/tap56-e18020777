
package practicas.hilos;

public class SleepMessages {
    public static void main(String args[])
        throws InterruptedException {
        String importantInfo[] = {
           "HIGH achiever don't you see?",
            "Curiquitacati",
            "Curiquitacata",
            "El dinero se cambia por productos y servicios"
        };

        for (int i = 0; i < importantInfo.length;  i++) {
            //Pause for 4 seconds
            Thread.sleep(4000);
            //Print a message
            System.out.println(importantInfo[i]);
        }
    }
}