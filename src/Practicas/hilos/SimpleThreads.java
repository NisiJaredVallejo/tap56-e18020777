
package practicas.hilos;


public class SimpleThreads {
    // Display a message, preceded by
    // the name of the current thread
    static void threadMessage(String message) {
        String threadName = Thread.currentThread().getName();
        System.out.format("%s: %s%n", threadName, message);
    }

    private static class MessageLoop implements Runnable {
        int tiempo = 0;
        
        public MessageLoop(int tiempo){
            this.tiempo = tiempo;
        }
        
        public void run() {
            String importantInfo[] = {
                "Las yeguas comen avena",
                "Does comen avena",
                "Pequeños chivos comen hierba",
                "Un chico comera hierba tambien"
            };
            try {
                for (int i = 0; i < importantInfo.length; i++) {
                    // Pause for 4 seconds
                    Thread.sleep(this.tiempo);
                    // Print a message
                    threadMessage(importantInfo[i]);
                }
            } catch (InterruptedException e) {
                threadMessage("Terminado!");
            }
        }
    }

    public static void main(String args[])
        throws InterruptedException {

        // Delay, in milliseconds before
        // we interrupt MessageLoop
        // thread (default one hour).
        long paciencia = 1000 * 60;

        // If command line argument
        // present, gives paciencia
        // in seconds.
        /*
        if (args.length > 0) {
            try {
                paciencia = Long.parseLong(args[0]) * 1000;
            } catch (NumberFormatException e) {
                System.err.println("El argumento debe ser entero.");
                System.exit(1);
            }
        }
        */

        threadMessage("Iniciando hilo MessageLoop");
        long startTime = System.currentTimeMillis();
        
        Thread t1 = new Thread(new MessageLoop(4000));
        t1.start();
        
        Thread t2 = new Thread(new MessageLoop(6000));
        t2.start();
        
        threadMessage("Esperando a que el hilo MessageLoop termine");
        // loop until MessageLoop
        // thread exits
        while (t1.isAlive() || t2.isAlive()) {
            threadMessage("Sigue esperando...");
            // Wait maximum of 1 second
            // for MessageLoop thread
            // to finish.
            t1.join(1000);
            t2.join(1000);
            if (((System.currentTimeMillis() - startTime) > paciencia)
                  && (t1.isAlive()|| t2.isAlive())) {
                threadMessage("Cansado de esperar!");
                t1.interrupt();
                t2.interrupt();
                // Shouldn't be long now
                // -- wait indefinitely
                t1.join(); t2.join();
            }
        }
        threadMessage("Terminado!");
    }
}
